# Home devices
## Init repository

Please read readme.md from github.com/tomasz2101/tools

### SUBMODULE INIT
git submodule add git@github.com:tomasz2101/tools.git tools
git submodule update --init --recursive --remote


# Deployment
## Kubernetes
All files used with k8s deployment are located in deployment dir, e.g:

    kubectl apply -k overlays/local

K8s deployment is used with kustomize tool which is integrated in kubectl



https://www.hackster.io/OxygenLithium/particulater-air-quality-monitoring-for-everyone-3caef2

# Usefull tools/commands
## mqtt:

    mosquitto_sub -h XXX -u XXX -P XXX  -t '#' -v

    mosquitto_pub -h XXX -u XXX -P XXX  -t XXX -m XXX

## virtual env
    apt-get install python3-venv
    python3 -m venv .venv
    source .venv/bin/activate
    pip3 install -r requirements-rpi.txt




# GPIO layout
    gpio
    led1 = gpio18
    led2 = gpio12
    temp = gpio4/7
    temp ds18 = gpio17

# prepare rpi
/boot/config.txt
    dtoverlay=w1-gpio,gpiopin="17"


# prepare rpi

## DS18B20
    add line to file /boot/config.txt
    dtoverlay=w1-gpio,gpiopin="17"



# to bbe able to build image on linux
https://github.com/multiarch/qemu-user-static#getting-started





# kindara


led green GPIO6
led yello GPIO13
led red GPIO19
bbuzzer GPIO18
button GPIO27
ds18 GPIO17
