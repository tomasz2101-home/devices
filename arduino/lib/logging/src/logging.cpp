#include <Arduino.h>
#include <logging.h>

void Logging::info(String text) {
  if (log_level >= 10) {
    Serial.println("INFO:    " + text);
  }
}
void Logging::debug(String text) { Serial.println("DEBUG:    " + text); }

void Logging::setupLogLevel(String log_level) {
  if (log_level == "debug") {
    this->log_level = 0;
    Serial.println("Logging LEVEL = debug");
  } else if (log_level == "info") {
    this->log_level = 10;
    Serial.println("Logging LEVEL = info");
  } else if (log_level == "warning") {
    this->log_level = 20;
    Serial.println("Logging LEVEL = warning");
  } else if (log_level == "error") {
    this->log_level = 30;
    Serial.println("Logging LEVEL = error");
  }
}
