#include <Arduino.h>

class Logging {
 public:
  int log_level = 10;
  void setupLogLevel(String text);
  void info(String text);
  void debug(String text);
};