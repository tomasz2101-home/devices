#include <Arduino.h>
#include <logging.h>

Logging logging;
void setup() {
  logging.setupLogLevel("info");
  Serial.begin(115200);

  logging.info("Setting up device ...");
  // connect_to_wifi();
  // client.setServer(MQTT_SERVER, MQTT_PORT);
  // setup_time();
  // EEPROM.begin(512);
  // init_mqtt();
  // execute_normal_work();
}

void loop() {
  logging.debug("Checking for new software");
  Serial.println("test");
  delay(1000);
}

// void execute_normal_work()
// {
//     if (UPLOAD_METHOD == 1)
//     {
//         log_info("Checking for new software");
// #if DEVICE_TYPE == 12
//         check_for_latest_version();
// #endif
//     }
//     if (SENSORS_MODE == 1)
//     {
//         log_info("Reporting only DS18B20 sensor data");
//         report_mqtt_sensor_data(get_mode_1());
//     }
//     else if (SENSORS_MODE == 2)
//     {
//         log_info("Reporting only dht11 sensor data");
//         report_mqtt_sensor_data(get_mode_2(false));
//     }
//     else if (SENSORS_MODE == 3)
//     {
//         log_info("Reporting DS18B20 and dht11 sensors data");
//         report_mqtt_sensor_data(get_mode_2(true));
//     }
//     else if (SENSORS_MODE == 4)
//     {
//         log_info("Reporting dht11 and water soil moisture data");
//         report_mqtt_sensor_data(get_mode_4());
//     }
//     if (UPLOAD_METHOD == 1)
//     {
//         log_info("Going into deep sleep for " + String(deep_sleep_time) + "
//         seconds"); delay(1000); // dont ever touch this deepsleep will stop
//         mqtt ESP.deepSleep(deep_sleep_time * 1000000);
//     }
// }