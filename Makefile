SHELL := /bin/bash
IMAGES := \
	devices \


include $(dir $(lastword ${MAKEFILE_LIST}))/tools/Makefile



secrets: lpass
	python3 tools/prepare_secrets.py --loglevel debug --input_file micro/secrets.json.tmpl --output_file micro/secrets.json.secret

init:
	pre-commit install
	python3 -m venv .venv
	source .venv/bin/activate && pip3 install -r requirements.txt

prepare/rpi:
	# Disable serial terminal over /dev/ttyAMA0
	sudo raspi-config nonint do_serial 1

	# Enable serial port
	raspi-config nonint set_config_var enable_uart 1 /boot/config.txt
	cat "dtoverlay=pi3-miniuart-bt" >> /boot/config.txt


pip3:
	pip3 install RPi.GPIO
	pip3 install pms5003

rsync:
	while inotifywait -r -e modify,create,delete,move .; do
		rsync -rtu --delete . tom@10.5.23.85:/tmp/test
	done
