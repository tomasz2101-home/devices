#!/usr/bin/env python3

import sys
import time
import argparse
import logging
import os
from module import helpers
from module.device import TempLogger


def parse_args(args):
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--raspberry', '-r', action='store_true')
    parser.add_argument('--kindara-username', '-u',
                        help='kindara app username/email')
    parser.add_argument('--kindara-password', '-p', help='kindara password')
    parser.add_argument('--loglevel', '-l', default='info', help='logs level')
    return parser.parse_args(args)


def main(*args):
    arguments = parse_args(args)
    helpers.set_logger(log_level=arguments.loglevel)
    logging.info("Starting...")
    device = TempLogger(args=arguments)
    device.run()


if __name__ == "__main__":
    main(*(sys.argv[1:]))
