#!/usr/bin/env python3

import sys
import time
import argparse
import logging
import os
from module import helpers
from module import newmqttclient
from module.device import Device


def parse_args(args):
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--raspberry', '-r', action='store_true')
    parser.add_argument('--mqtt-url', default='localhost',
                        help='mqtt server url')
    parser.add_argument('--mqtt-port', type=int,
                        default=1883, help='mqtt server port')
    parser.add_argument('--mqtt-username', default='user',
                        help='mqtt username')
    parser.add_argument('--mqtt-password',
                        default=os.getenv("MQTT_PASSWORD", default='password'),
                        help='mqtt password')

    parser.add_argument('--loglevel', '-l', default='info', help='logs level')

    parser.add_argument('--sensors', '-s', type=str, default=[], nargs='+',
                        help='which sensors to use')
    parser.add_argument('--leds-pin', type=int, default=12,
                        help='leds pin')
    parser.add_argument('--leds-number', type=int, default=30,
                        help='leds number')
    parser.add_argument('--dht-pin', type=int, default=4,
                        help='dht sensor pin')

    parser.add_argument(
        '--device-name', default=helpers.get_hostname(), help='device name')
    return parser.parse_args(args)


def main(*args):
    arguments = parse_args(args)
    helpers.set_logger(log_level=arguments.loglevel)
    logging.info("Starting...")
    device = Device(args=arguments)
    device.run()


if __name__ == "__main__":
    main(*(sys.argv[1:]))
