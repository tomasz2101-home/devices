import argparse
import logging
import socket
import platform


# class MissingPassword(Exception):
#     def __init__(self):
#         super(MissingPassword, self).__init__(
#             "No password/s. Please read helper (-h)")


def set_logger(log_level: str):
    logging.basicConfig(
        format='%(asctime)s %(levelname)s: %(message)s', datefmt='%Y-%m-%dT%H:%M:%S')
    logger = logging.getLogger()
    logger.setLevel(log_level.strip().upper())


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except KeyError:
        IP = '127.0.0.1'
    finally:
        s.close()
    return str(IP)


def get_hostname():
    return platform.node()


def translate(value, range_in, range_out):
    leftSpan = range_in[1] - range_in[0]
    rightSpan = range_out[1] - range_out[0]
    valueScaled = float(value - range_in[0]) / float(leftSpan)
    return range_out[0] + (valueScaled * rightSpan)


# def message_to_dict(self, message: bytes) -> dict:
#     return ast.literal_eval(self.message_to_str(message))
