from module import helpers

import paho.mqtt.client as mqtt  # type: ignore
import paho.mqtt.publish as publish  # type: ignore
import logging
import time
import json
from datetime import datetime
import struct
import ast
import sys


class MyMQTTClass(mqtt.Client):
    def __init__(self, broker_url: str = 'localhost', port: int = 1883, user: str = 'admin', password: str = 'password'):
        mqtt.Client.__init__(self)
        self._broker_url = broker_url
        self._port = port
        self._user = user
        self._password = password
        self._protocol = mqtt.MQTTv311
        self._transport = 'tcp'
        self._last_topic = '#'
        self._last_message = ''
        self._connected_flag = False
        self._bad_connection_flag = False
        self.start()

    def get_last_message(self):
        return self._last_message

    def get_last_topic(self):
        return self._last_topic

    def check_connection(self):
        while not self._connected_flag and not self._bad_connection_flag:
            logging.info("In wait loop")
            time.sleep(1)
        if self._bad_connection_flag:
            logging.error('Failed to connect')
            self.loop_stop()
            sys.exit()

    def start(self):
        self.username_pw_set(username=self._user, password=self._password)
        self.connect(self._broker_url, self._port, 60)
        self.loop_start()
        timeout = 0
        while not self._connected_flag and timeout < 10:
            logging.debug('Waiting for connection...')
            time.sleep(1)
            timeout += 1
        if timeout > 9:
            logging.error('Connection to mqtt broker timeout')

    def on_connect(self, mqttc, obj, flags, rc):
        if rc == 0:
            self._connected_flag = True
            logging.info("Connected")
        else:
            self._bad_connection_flag = True
            logging.error("Bad connection Returned code=" + rc)

    def on_message(self, mqttc, obj, msg):
        logging.debug(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))

    def on_publish(self, mqttc, obj, mid):
        logging.debug("mid: " + str(mid))

    def listen(self, topic: str = '#'):
        self._last_topic = topic
        self.subscribe(self._last_topic, 0)

    def on_subscribe(self, mqttc, obj, mid, granted_qos):
        logging.info('Subscribed to {}'.format(self._last_topic))

    def on_disconnect(client, packet, exc=None):
        logging.info('Disconnected')

    def publish(self, topic: str, payload: str):
        self._last_topic = topic
        self._last_message = payload
        publish.single(topic,
                       payload=payload,
                       qos=0,
                       retain=False,
                       hostname=self._broker_url,
                       port=self._port,
                       client_id="",
                       keepalive=60,
                       will=None,
                       auth={'username': self._user,
                             'password': self._password
                             },
                       tls=None,
                       protocol=self._protocol,
                       transport=self._transport)

    def prepare_message(self, message: str, data: dict) -> str:
        message_ouput = {
            "Time": datetime.now().strftime("%Y:%m:%dT%H:%M:%S"),
            "ip": helpers.get_ip(),
            "host": helpers.get_hostname(),
            "message": message,
            "data": data
        }
        return json.dumps(message_ouput)

    def send_message(self, topic, message, data={}, binData=None):
        self.publish(topic, payload=self.prepare_message(message, data))

    def message_to_str(self, message: bytes) -> str:
        return str(message, 'utf-8')

    def message_to_dict(self, message: bytes) -> dict:
        return ast.literal_eval(self.message_to_str(message))

    def translate_error_code(self, code: int) -> str:
        codes = {0: 'Connection successful',
                 1: 'Connection refused – incorrect protocol version',
                 2: 'Connection refused – invalid client identifier',
                 3: 'Connection refused – server unavailable',
                 4: 'Connection refused – bad username or password',
                 5: 'Connection refused – not authorised'}
        return codes[code]
