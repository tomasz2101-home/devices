import logging
import time
import glob
from module import helpers
from random import randint
import collections

try:
    import RPi.GPIO as GPIO  # type: ignore
except ImportError:
    logging.warning("Failed import RPi.GPIO as GPIO")

try:
    import neopixel  # type: ignore
except ImportError:
    logging.warning("Failed import neopixel")

try:
    from pms5003 import PMS5003  # type: ignore
except ImportError:
    logging.warning("Failed from pms5003 import PMS5003")

try:
    import Adafruit_DHT  # type: ignore
except ImportError:
    logging.warning("Failed import Adafruit_DHT")


class FakeButton:
    def detect_event(self):
        input("Press Enter to continue...")
        return True


class Button(FakeButton):
    def __init__(self, setup_button: bool = False):
        self._pin = 27
        GPIO.setup(self._pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def detect_event(self,):
        while True:
            if GPIO.input(self._pin) == False:
                print("test...")
                return True


class FakeLeds:
    def turn_on(self, red: int = 0, green: int = 0, blue: int = 0):
        logging.info("Turn on")

    def turn_off(self):
        logging.info("Turn off")

    def setup(self, config: dict):
        logging.debug('Setup leds')


class Leds(FakeLeds):
    def __init__(self, pin: int = 12, leds_number: int = 30):
        if pin is not 12 or not 18:
            logging.error("You can choose only between pins 12/18")
        else:
            self.id = pin
        self._leds = neopixel.NeoPixel(self, leds_number)
        self._colors = (0, 0, 0)
        self._brightness = 255
        self._effect = 'solid'
        self._state = False

    def turn_on(self):
        logging.info('Turn on')
        self._leds.brightness = helpers.translate(
            self._brightness, [0, 255], [0, 1])
        self._leds.fill(self._colors)
        self._state = True

    def turn_off(self):
        logging.info('Turn off')
        self._leds.fill((0, 0, 0))
        self._state = False

    def toggle(self):
        if self._state:
            self.turn_off()
        else:
            self.turn_on()

    def setup(self, config: dict):
        try:
            self._colors = (config['color']['r'],
                            config['color']['g'], config['color']['b'])
        except KeyError:
            logging.debug('No colors in message skipping ...')
        try:
            self._brightness = config['brightness']
        except KeyError:
            logging.debug('No brightness in message skipping ...')
        try:
            self._effect = config['effect']
        except KeyError:
            logging.debug('No effect in message skipping ...')


class FakePMS:
    def get_pm_ug_per_m3(self):
        return {'fake_1.0': 1,
                'fake_2.5': 2,
                'fake_10.0': 3}


class PMS(FakePMS):
    def __init__(self, pms_port: int = 18):
        self._pms_port = pms_port
        self._pms5003 = PMS5003()

    def enable_ports(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self._pms_port, GPIO.OUT)

    def turn_on(self):
        GPIO.output(self._pms_port, GPIO.HIGH)

    def turn_off(self):
        GPIO.output(self._pms_port, GPIO.LOW)

    def get_all(self):
        return self._pms5003.read()

    def get_specific(self, particle_size: float = 1.0):
        data = self._pms5003.read()
        return data.pm_ug_per_m3(particle_size)

    def get_pm_ug_per_m3(self):
        data = self._pms5003.read()
        return {'1.0': data.pm_ug_per_m3(size=1.0),
                '2.5': data.pm_ug_per_m3(size=2.5),
                '10.0': data.pm_ug_per_m3(size=10.0)}


class FakeDHT:
    def read_x_times(self, repeats: int = 10, delay: int = 2):
        humidity = 0
        temperature = 0
        for x in range(0, repeats):
            humidity_read, temperature_read = [5, 55]
            humidity += humidity_read
            temperature += temperature_read
        return {'fake_temperature': temperature / repeats, 'fake_humidity': humidity / repeats}


class DHT(FakeDHT):
    def __init__(self, pin: int = 4):
        self._pin = pin
        self._sensor_type = Adafruit_DHT.DHT11

    def read_x_times(self, repeats: int = 10, delay: int = 5):
        humidity = 0
        temperature = 0
        for x in range(0, repeats):
            humidity_read, temperature_read = Adafruit_DHT.read_retry(
                self._sensor_type, self._pin)
            humidity += humidity_read
            temperature += temperature_read
            time.sleep(delay)
        return {'temperature': temperature / repeats, 'humidity': humidity / repeats}


class FakeDS18B20:
    def read_temp(self) -> float:
        return randint(36, 37)


class DS18B20(FakeDS18B20):
    def __init__(self):
        self.base_dir = '/sys/bus/w1/devices/'
        self.device_folder = glob.glob(self.base_dir + '28*')[0]
        self.device_file = self.device_folder + '/w1_slave'

    def read_temp_raw(self) -> list:
        f = open(self.device_file, 'r')
        lines = f.readlines()
        f.close()
        return lines

    def read_temp(self) -> float:
        lines: list = self.read_temp_raw()
        i: int = 0
        while lines[0].strip()[-3:] != 'YES' and i < 5:
            time.sleep(0.2)
            lines = self.read_temp_raw()
            ++i
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos + 2:]
            return (float(temp_string) / 1000.0) * (36.54 / 36.17)
        return False


class MeasurementRing:
    def __init__(self, count: int = 10):
        self._count = count
        self._ring: list = []

    def add(self, value):
        if len(self._ring) >= self._count:
            del self._ring[0]
        self._ring.append(value)

    def get_full(self) -> list:
        return self._ring

    def clean(self):
        self._ring = []
