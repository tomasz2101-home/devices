import requests
import logging
import datetime
import pytz


class Kindara:
    def __init__(self, username: str = 'username', password: str = 'password'):
        self._username: str = username
        self._password: str = password
        self._token: str = ""
        self._url = "https://kindara-app.appspot.com/api"
        self._timezone = "Europe/Warsaw"
        self.authenticate()

    def authenticate(self):
        files = {
            'email': (None, self._username),
            'password': (None, self._password),
        }
        response = requests.post(self._url + '/account.auth', files=files)
        logging.debug(response.json())
        try:
            self._token = str(response.json()["token"])

        except KeyError:
            logging.error("Couldn't get kindara token")
            exit(0)

    def update_day(self, temperature: float):
        now = datetime.datetime.now(pytz.timezone(self._timezone))
        temperature_fahrenheit: float = 9.0 / 5.0 * temperature + 32
        files = {
            'token': (None, self._token),
            'date': (None, str(now.strftime("%Y-%m-%d"))),
            'temperature': (None, str(temperature_fahrenheit)),
            'temp_time': (None, now.strftime("%H:%M")),
            'updated': (None, str(now.isoformat()))
        }

        response = requests.post(self._url + '/day.push', files=files)
        logging.debug(response.json())
        try:
            if response.json()['msg'] != 'success':
                logging.error('Failed to send correct data')
            else:
                logging.info("Message sucefully send")
        except KeyError:
            logging.error('Failed to send correct data')
            exit(0)

    def get_day(self, date: str) -> object:
        files = {
            'token': (None, self._token),
        }
        response = requests.post(self._url + '/day.pull', files=files)
        logging.debug([x for x in response.json()[
                      'days'] if x['date'] == date])
        return [x for x in response.json()['days'] if x['date'] == date]
