from module import sensors
from module import newmqttclient
import time
import logging
from typing import Any
from module import kindara


class TempLogger:
    def __init__(self, args):
        self._ds18b20 = sensors.FakeDS18B20()
        self._button = sensors.FakeButton()
        self._ring = sensors.MeasurementRing(count=10)
        # self._kindara = kindara.Kindara(username=args.kindara_username,
        #                                 password=args.kindara_password)
        if args.raspberry:
            self._ds18b20 = sensors.DS18B20()
            self._button = sensors.Button()

    def run(self):
        while True:
            if self._button.detect_event():
                start = time.time()
                while True:
                    temperature: float = self._ds18b20.read_temp()
                    self._ring.add(temperature)
                    print(self._ring.get_full())
                    if len(set(self._ring.get_full())) == 1 and len(self._ring.get_full()) > self._ring._count - 1:
                        print("%.2f" % next(iter(set(self._ring.get_full()))))
                        self._ring.clean()
                        end = time.time()
                        print(end - start)
                        break

                # self._kindara.update_day(temperature=self._ds18b20.read_temp())


class Device:
    def __init__(self, args):
        self._sensors_list = args.sensors
        self._name = args.device_name

        self._air_quality_sensor = sensors.FakePMS()
        self._dht_sensor = sensors.FakeDHT()
        self._leds = sensors.FakeLeds()
        self._ds18b20 = sensors.FakeDS18B20()
        if args.raspberry:
            if 'pms' in self._sensors_list:
                self._air_quality_sensor = sensors.PMS()
            if 'dht' in self._sensors_list:
                self._dht_sensor = sensors.DHT(pin=args.dht_pin)
            if 'leds' in self._sensors_list:
                self._leds = sensors.Leds(
                    pin=args.leds_pin, leds_number=args.leds_number)
            if 'ds18b20' in self._sensors_list:
                self._ds18b20 = sensors.DS18B20()
        # if args.mqtt_url:
        self._mqtt = newmqttclient.MyMQTTClass(broker_url=args.mqtt_url,
                                               port=args.mqtt_port,
                                               user=args.mqtt_username,
                                               password=args.mqtt_password)
        self._sleep_time = 5

    # def validate(self, mosq, obj, msg):
    #     print('testing')
    #     print(self._mqtt.get_last_message())
    #     print(self._mqtt.get_last_topic())
    #     print('...')
    #     print(msg.topic)
    #     print(msg.payload)
    #     # if msg.topic ==
    #     # exit(1)

    def run_leds_mode(self, mosq, obj, msg):
        payload = self._mqtt.message_to_dict(message=msg.payload)
        self._leds.setup(config=payload)
        return_topic = msg.topic[:-len('/set')]
        if payload['state'] == 'ON':
            self._leds.turn_on()
            self._mqtt.publish(topic=return_topic,
                               payload=self._mqtt.message_to_str(message=msg.payload))
            # self._mqtt.message_callback_add(return_topic, self.validate)
        elif payload['state'] == 'OFF':
            self._leds.turn_off()
            self._mqtt.publish(topic=return_topic,
                               payload=self._mqtt.message_to_str(message=msg.payload))

    def run(self):
        self._mqtt.check_connection()

        if 'leds' in self._sensors_list:
            self._mqtt.message_callback_add('home/{name}/led_01/set'.format(name=self._name),
                                            self.run_leds_mode)
            # TODO enable other leds on same pin
            # self._mqtt.message_callback_add('home/{name}/led_02'.format(name=self._name), self.run_leds_mode)
        self._mqtt.subscribe("#")
        while True:
            sensors = dict()
            if 'dht' in self._sensors_list:
                sensors['dht'] = self._dht_sensor.read_x_times()
            if 'pms' in self._sensors_list:
                sensors['pms'] = self._air_quality_sensor.get_pm_ug_per_m3()
            if 'ds18b20' in self._sensors_list:
                sensors['ds18b20'] = self._ds18b20.read_temp()
            if sensors:
                self._mqtt.send_message(topic='home/{name}/sensors'.format(name=self._name),
                                        message='Sensors: {}'.format(
                                            '/'.join(list(sensors.keys()))),
                                        data=sensors)
            time.sleep(self._sleep_time)
