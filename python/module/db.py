import time
import logging
import psycopg2  # type: ignore


class Postgres:
    def __init__(self, url: str = "localhost", port: int = 5432, name: str = "ania", user: str = "postgres", password: str = "mysecretpassword"):
        self.connection = psycopg2.connect(
            database=name, user=user, password=password, host=url, port=port)
        self.create_table()

    def create_table(self):
        cur = self.connection.cursor()
        try:
            cur.execute('''create table temperature
                (ID     SERIAL PRIMARY KEY,
                TIME    INT    NOT NULL,
                VALUE   INT    NOT NULL);''')
            self.connection.commit()
        except psycopg2.errors.DuplicateTable:
            print("Test")
        cur.execute("ROLLBACK")

    def add(self):
        cur = self.connection.cursor()
        try:
            cur.execute(
                "insert into temperature (id, time, value) values (default, 1231, 123)")
            self.connection.commit()

        except psycopg2.errors.DuplicateTable:
            print("Test")
