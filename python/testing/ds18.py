import time
import glob


class DS18B20():
    def __init__(self):
        self.base_dir = '/sys/bus/w1/devices/'
        self.device_folder = glob.glob(self.base_dir + '28*')[0]
        self.device_file = self.device_folder + '/w1_slave'

    def read_temp_raw(self) -> list:
        f = open(self.device_file, 'r')
        lines = f.readlines()
        f.close()
        return lines

    def read_temp(self) -> float:
        lines: list = self.read_temp_raw()
        i: int = 0
        while lines[0].strip()[-3:] != 'YES' and i < 5:
            time.sleep(0.2)
            lines = self.read_temp_raw()
            ++i
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos + 2:]
            return float(temp_string) / 1000.0
        return False


test = DS18B20()
print(test.read_temp())
