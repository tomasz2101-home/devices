#!/usr/bin/python3

import psycopg2

con = psycopg2.connect(database="ania", user="postgres",
                       password="mysecretpassword", host="127.0.0.1", port="5432")

print("Database opened successfully")

cur = con.cursor()
try:
    cur.execute('''create table temperature
        (ID     SERIAL PRIMARY KEY,
        TIME    INT    NOT NULL,
        VALUE   INT    NOT NULL);''')
    con.commit()
except psycopg2.errors.DuplicateTable:
    print("Test")
cur.execute("ROLLBACK")

try:
    cur.execute(
        "insert into temperature (id, time, value) values (default, 1231, 123)")
    con.commit()
except psycopg2.errors.DuplicateTable:
    print("Test")

con.close()
