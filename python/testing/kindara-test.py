#!/usr/bin/env python3

from module.kindara import Kindara
from module import helpers
import sys


def main(*args):

    helpers.set_logger(log_level="info")
    test = Kindara(username="XXX", password="XXX")

    test.update_day(temperature=37)
    test.get_day(date='2019-12-12')


if __name__ == "__main__":
    main(*(sys.argv[1:]))
