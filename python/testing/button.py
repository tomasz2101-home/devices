#!/usr/bin/python3

import time

import RPi.GPIO as GPIO

GPIO.setwarnings(False)  # Ignore warning for now
GPIO.setmode(GPIO.BCM)  # Use physical pin numbering
# Set pin 10 to be an input pin and set initial value to be pulled low (off)
GPIO.setup(27, GPIO.IN, pull_up_down=GPIO.PUD_UP)
while True:  # Run forever
    if GPIO.input(27) == False:
        print('Button Pressed')
        time.sleep(0.2)
