import uasyncio as asyncio
import time


async def send_dht(duration):
    while True:
        print("send_dht")
        await asyncio.sleep(duration)


async def take_care_of_mqtt(time_ms):
    while True:
        await asyncio.sleep_ms(time_ms)
        print('take_care_of_mqtt')


loop = asyncio.get_event_loop()
loop.create_task(send_dht(1000))
loop.run_until_complete(take_care_of_mqtt(10))
