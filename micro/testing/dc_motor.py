from machine import Pin, PWM

""" nodemcu pins from the motor shield """
pin1 = Pin(5, Pin.OUT)  # D1
pin2 = Pin(4, Pin.OUT)  # D2


""" named after the L9110 h-bridge pins """
BIN1 = PWM(pin1, freq=750)

AIN1 = PWM(pin2, freq=750)


""" TODO: variable speed """
speed = 950 

def stop_all():
    for each in (BIN1, AIN1):
        each.duty(0)


def forward():
    BIN1.duty(speed)

    AIN1.duty(speed)


forward()