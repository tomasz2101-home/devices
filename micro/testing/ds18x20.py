import time
import machine
import onewire
import ds18x20

# the device is on GPIO12


# scan for devices on the bus


def get_resolution(self):
    self._reset()
    scratchpad = self._read_scratchpad()
    return (iord(scratchpad, 4) >> 5) & 0b11

# loop 10 times and print all temperatures


def main():
    dat = machine.Pin(0)

    # create the onewire object
    ds = ds18x20.DS18X20(onewire.OneWire(dat))
    roms = ds.scan()
    print('found devices:', roms)
    while True:
        print('temperatures:', end=' ')
        ds.convert_temp()
        time.sleep_ms(750)
        for rom in roms:
            print(ds.read_temp(rom), end=' ')
        print()
