import stepper
from machine import Pin
import time

test = stepper.Stepper(step_pin=Pin(12), dir_pin=Pin(14), sleep_pin=Pin(0))
while True:
    test.rel_angle(90)
    time.sleep(1)
    test.rel_angle(-90)
    time.sleep(1)
