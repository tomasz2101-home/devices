import time
import board
import neopixel


class Leds:
    def __init__(self):
        self.leds = neopixel.NeoPixel(board.GPIO15, 30)

    # def set_leds(self, red, green, blue, brightness):
    #     r = int(red * brightness)
    #     g = int(green * brightness)
    #     b = int(blue * brightness)
    #     for i in range(self.leds.n):
    #         self.leds[i] = (r, g, b)
    #     self.leds.write()

    def testing(self):
        # time.sleep(1)
        self.leds(255, 255, 255, 1)
        time.sleep(1)
        self.set_leds(255, 100, 100, 1)
        time.sleep(1)
        self.set_leds(255, 0, 0, 1)
        time.sleep(1)
        self.set_leds(0, 0, 0, 0)


t = Leds()
t.testing()
# import testing.leds
# testing.leds.Leds().testing()
