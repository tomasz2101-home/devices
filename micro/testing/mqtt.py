from umqtt.simple import MQTTClient
from machine import Pin
import ubinascii
import machine
import micropython


# ESP8266 ESP-12 modules have blue, active-low LED on GPIO2, replace
# with something else if needed.
led = Pin(2, Pin.OUT, value=1)

# Default MQTT server to connect to
CLIENT_ID = ubinascii.hexlify(machine.unique_id())


state = 0


def sub_cb(topic, msg):
    global state
    print((topic, msg))
    if msg == b"on":
        led.value(0)
        state = 1
    elif msg == b"off":
        led.value(1)
        state = 0
    elif msg == b"toggle":
        # LED is inversed, so setting it to current state
        # value will make it toggle
        led.value(state)
        state = 1 - state


def main():
    c = MQTTClient(client_id='id1', server='m23.cloudmqtt.com',
                   port=10252, user='tybwpytq', password='ysx9nOXM3lJ0')
    c.connect()
    c.set_callback(sub_cb)

    c.subscribe("led")
    print("Connected to %s, subscribed to %s topic" %
          ('m23.cloudmqtt.com', "led"))

    # try:
    while 1:
        print("Testing")
        # micropython.mem_info()
        # c.wait_msg()
    # finally:
    #     c.disconnect()
