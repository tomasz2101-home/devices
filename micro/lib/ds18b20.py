import onewire
import machine
from micropython import const
import time

_CONVERT = const(0x44)
_RD_SCRATCH = const(0xbe)
_WR_SCRATCH = const(0x4e)
FAMILY_CODE = 0x28
RES_9_BIT = 0x0
RES_10_BIT = 0x1
RES_11_BIT = 0x2
RES_12_BIT = 0x3


def bytesarray2bytes(array): return bytes(array)


def iord(buf, i): return buf[i]


class FakeDS18X20:
    def get_temperature(self):
        return 123.123


class DS18X20:
    def __init__(self, pin: int, sensors_address: int = 0):
        self.ow = onewire.OneWire(machine.Pin(pin))
        self.buf = bytearray(9)
        self.sensor = self.ow.scan()[sensors_address]

    def scan(self):
        return [rom for rom in self.ow.scan() if rom[0] in (0x10, 0x22, 0x28)]

    def info(self):
        print('Address: ', str(self.sensor))
        print('Address: ', str(self.get_resolution()))

    def get_resolution(self):
        self._reset()
        scratchpad = self.read_scratch()
        return (iord(scratchpad, 4) >> 5) & 0b11

    def _reset(self):
        self.ow.reset(True)

    def set_resolution(self, resolution):
        resolution &= 0b11
        self._reset()
        scratchpad = self.read_scratch()
        raw = bytesarray2bytes([
            iord(scratchpad, 2),
            iord(scratchpad, 3),
            (resolution << 5) | 0b00011111
        ])
        self._reset()
        self.write_scratch(buf=raw)

    def convert_temp(self):
        self.ow.reset(True)
        self.ow.writebyte(self.ow.SKIP_ROM)
        self.ow.writebyte(_CONVERT)

    def read_scratch(self, rom=None):
        if not rom:
            rom = self.sensor
        self.ow.reset(True)
        self.ow.select_rom(rom)
        self.ow.writebyte(_RD_SCRATCH)
        self.ow.readinto(self.buf)
        if self.ow.crc8(self.buf):
            raise Exception('CRC error')
        return self.buf

    def write_scratch(self, buf, rom=None):
        if not rom:
            rom = self.sensor
        self.ow.reset(True)
        self.ow.select_rom(rom)
        self.ow.writebyte(_WR_SCRATCH)
        self.ow.write(buf)

    def read_temp(self, rom=None):
        if not rom:
            rom = self.sensor
        buf = self.read_scratch(rom)
        if rom[0] == 0x10:
            if buf[1]:
                t = buf[0] >> 1 | 0x80
                t = -((~t + 1) & 0xff)
            else:
                t = buf[0] >> 1
            return t - 0.25 + (buf[7] - buf[6]) / buf[7]
        else:
            t = buf[1] << 8 | buf[0]
            if t & 0x8000:  # sign bit set
                t = -((t ^ 0xffff) + 1)
            return t / 16

    def get_temperature(self, rom=None):
        self.convert_temp()
        time.sleep(1)
        return self.read_temp(rom=rom)
