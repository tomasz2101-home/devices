from machine import Timer
import time


class PIR():

    def __init__(self, pin, reactivation_delay_ms=1000):
        self._pin = pin
        self.active = False
        self._waiting_reactivation = False
        self.reactivation_delay = reactivation_delay_ms
        self._callback_on = None
        self._callback_off = None
        self._qtimer = Timer(501)  # 501 = timerID (from sensor model number)
        self._qtimer.init(period=200, mode=Timer.PERIODIC, callback=self._monitor)
        self._last_detection = None

    def _monitor(self, _=None):
        is_active_now = self.is_active(raw=True)
        if not self.active and is_active_now:
            self.active = True
            if self._callback_on:
                self._callback_on()
            self._last_detection = time.ticks_ms()
            self._waiting_reactivation = False
        elif self.active:
            now = time.ticks_ms()
            if not is_active_now:
                self._waiting_reactivation = True
            elif self._waiting_reactivation:
                self._last_detection = now
                self._waiting_reactivation = False

            if time.ticks_diff(now, self._last_detection) >= self.reactivation_delay:
                if is_active_now:
                    self._last_detection = now - self.reactivation_delay // 2
                else:
                    self.active = False
                    self._waiting_reactivation = False
                    if self._callback_off:
                        self._callback_off()

    def clear(self):
        self._callback_on = None
        self._callback_off = None

    def init(self, pin, reactivation_delay_ms=1000):
        self._pin = pin
        self._prepare_monitor()

    def is_active(self, raw=False):
        return bool(self._pin()) if raw else self.active

    def on_movement_start(self, callback):
        self._callback_on = callback
        if callback is not None and self.is_active():
            self._monitor()

    def on_movement_end(self, callback):
        self._callback_off = callback
