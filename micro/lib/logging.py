import sys

CRITICAL = 50
ERROR = 40
WARNING = 30
INFO = 20
DEBUG = 10
NOTSET = 0

_level_dict = {
    CRITICAL: "CRIT",
    ERROR: "ERROR",
    WARNING: "WARN",
    INFO: "INFO",
    DEBUG: "DEBUG",
}


_nameToLevel = {
    'CRITICAL': CRITICAL,
    'ERROR': ERROR,
    'WARNING': WARNING,
    'INFO': INFO,
    'DEBUG': DEBUG,
    'NOTSET': NOTSET,
}

_stream = sys.stderr


def _checkLevel(level):
    if isinstance(level, int):
        rv = level
    elif str(level) == level:
        if level.strip().upper() not in _nameToLevel:
            raise ValueError("Unknown level: %r" % level)
        rv = _nameToLevel[level.strip().upper()]
    else:
        raise TypeError("Level not an integer or a valid string: %r" % level)
    return rv


class Logger:

    level = NOTSET

    def __init__(self, name):
        self.name = name

    def _level_str(self, level):
        l = _level_dict.get(level)
        if l is not None:
            return l
        return "LVL%s" % level

    def setLevel(self, level):
        self.level = _checkLevel(level)

    def isEnabledFor(self, level):
        return level >= (self.level or _level)

    def log(self, level, msg, *args):
        if level >= (self.level or _level):
            _stream.write("%s:%s:" % (self._level_str(level), self.name))
            if not args:
                print(msg, file=_stream)
            else:
                print(msg % args, file=_stream)

    def debug(self, msg, *args):
        self.log(DEBUG, msg, *args)

    def info(self, msg, *args):
        self.log(INFO, msg, *args)

    def warning(self, msg, *args):
        self.log(WARNING, msg, *args)

    def error(self, msg, *args):
        self.log(ERROR, msg, *args)

    def critical(self, msg, *args):
        self.log(CRITICAL, msg, *args)

    def exception(self, msg, *args):
        error(msg, *args)


_level = INFO
_loggers = {}  # type: ignore
root = Logger("root-logger")


def basicConfig(level: str = None, filename: str = None, format: str = None, **kwargs):
    global _level, _stream

    if level is not None:
        root.setLevel(level)

    stream = kwargs.pop("stream", None)
    if stream is not None:
        _stream = stream

    if filename is not None:
        print("logging.basicConfig: filename arg is not supported")

    if format is not None:
        print("logging.basicConfig: format arg is not supported")


def critical(msg, *args, **kwargs):
    root.critical(msg, *args, **kwargs)


fatal = critical


def error(msg, *args, **kwargs):
    root.error(msg, *args, **kwargs)


def exception(msg, *args, **kwargs):
    root.exception(msg, *args, **kwargs)


def warning(msg, *args, **kwargs):
    root.warning(msg, *args, **kwargs)


def info(msg, *args, **kwargs):
    root.info(msg, *args, **kwargs)


def debug(msg, *args, **kwargs):
    root.debug(msg, *args, **kwargs)
