import json
import logging
import time
import binascii
try:
    import ntptime  # pylint: disable=import-error
    import network  # pylint: disable=import-error
    import esp  # pylint: disable=import-error
except ModuleNotFoundError:
    logging.warning("Skipping importing some libararies")

import modules.configuration


start_time = 0


def since_start_seconds():
    return time.time() - start_time


def set_time():
    if network.WLAN(network.STA_IF).isconnected():
        ntptime.settime()


def get_time() -> str:
    now = time.localtime()
    return "{}:{}:{}T{}:{}:{}".format(now[0], now[1], now[2], now[3], now[4], now[5])


def no_debug():
    esp.osdebug(None)


def get_mac() -> str:

    return str(binascii.hexlify(network.WLAN().config('mac'), ':').decode())  # type: ignore


def get_ip() -> str:
    return str(network.WLAN(network.AP_IF).ifconfig()[0])


def translate(value, range_in, range_out):
    leftSpan = range_in[1] - range_in[0]
    rightSpan = range_out[1] - range_out[0]
    valueScaled = float(value - range_in[0]) / float(leftSpan)
    return range_out[0] + (valueScaled * rightSpan)


def message_to_dict(message: bytes) -> dict:
    return json.loads(message.decode('utf-8'))
