import neopixel  # pylint: disable=import-error
import logging
import modules.helpers
from modules.hardware import Wemos8266


class FakeLeds:
    def turn_on(self, red: int = 0, green: int = 0, blue: int = 0):
        logging.info("Turn on")

    def turn_off(self):
        logging.info("Turn off")

    def setup(self, config: dict):
        logging.info('Setup leds')


class Leds(FakeLeds):
    def __init__(self, pin: str = "D0", leds_number: int = 30, auto_write=True):
        self.leds = neopixel.NeoPixel(pin=Wemos8266(pin=pin), n=leds_number)
        self._red = 0  # max 255
        self._green = 0  # max 255
        self._blue = 0  # max 255

        self._brightness = 0  # max 255
        self._effect = 'solid'
        self._state = False
        self._auro_write = auto_write

    def set_leds(self, red: int = None, green: int = None, blue: int = None,
                 brightness=None):
        if red is None:
            red = self._red
        if green is None:
            green = self._green
        if blue is None:
            blue = self._blue
        if brightness is None:
            brightness = self._brightness
        brightness = modules.helpers.translate(brightness, [0, 255], [0, 1])
        r = int(self._red * brightness)
        g = int(self._green * brightness)
        b = int(self._blue * brightness)
        for i in range(self.leds.n):
            self.leds[i] = (r, g, b)
        if self._auro_write:
            self.leds.write()

    def turn_on(self):
        logging.debug('Turn on')
        self.set_leds()
        self._state = True

    def turn_off(self):
        logging.debug('Turn off')
        self.set_leds(brightness=0)
        self._state = False

    def toggle(self):
        if self._state:
            self.turn_off()
        else:
            self.turn_on()

    def setup(self, config: dict):
        try:
            self._red = config['color']['r']
            self._green = config['color']['g']
            self._blue = config['color']['b']
        except KeyError:
            logging.debug('No colors in message skipping ...')
        try:
            self._brightness = config['brightness']
        except KeyError:
            logging.debug('No brightness in message skipping ...')
        try:
            self._effect = config['effect']
        except KeyError:
            logging.debug('No effect in message skipping ...')
