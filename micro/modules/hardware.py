from machine import Pin, ADC  # pylint: disable=import-error
import modules.configuration



class GenericBoard():

    
    def __new__(self, config):
        if config['board'] == "esp32":
            return ESP32()
        # elif config['board'] == "wemos8266":
        #     return Wemos8266()
        

class Wemos8266():
    def __init__(self):
        self.pins = {
            "D0": 16,
            "D1": 5,
            "D2": 4,
            "D3": 0,
            "D4": 2,
            "D5": 14,
            "D6": 12,
            "D7": 13,
            "D8": 15,
            "A0": 0,
            "RX": 3,
            "TX": 1,
        }
        self.modes = {"IN": Pin.IN,
                    "OUT": Pin.OUT}
    
    def get_pin(self, pin, mode="OUT"):
        return Pin(self.pins[pin], self.modes[mode])

class ESP32():
    def __init__(self):
        self.modes = {"IN": Pin.IN,
                    "OUT": Pin.OUT}
    
    def get_pin(self, pin, mode="OUT"):
        return Pin(int(pin), self.modes[mode])

        