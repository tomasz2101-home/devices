
import json
import time
import logging
try:
    from umqtt.simple import MQTTClient  # pylint: disable=import-error
except ModuleNotFoundError:
    logging.warning("Skipping importing some libararies")

import modules.helpers


class FakeMyMQTT:
    def publish(self, topic: str, msg: str):
        logging.info("Fake send topic: {} message {}".format(topic, msg))

    def prepare_message(self, message: str, data: dict) -> str:
        message_ouput = {
            "time": modules.helpers.get_time(),
            "time_since_start": "%.2f" % modules.helpers.since_start_seconds(),
            "ip": modules.helpers.get_ip(),
            "host": modules.helpers.get_mac(),
            "message": message,
            "data": data
        }
        return json.dumps(message_ouput)

    def send_message(self, topic, message, data={}, binData=None):
        self.publish(topic=topic, msg=self.prepare_message(
            message=message, data=data))
    
    def check_msg(self):
        return True


class MyMQTT(MQTTClient, FakeMyMQTT):
    def __init__(self, client_name: str = "default-name", server: str = 'localhost', port: int = 1883,
                 user: str = 'admin', password: str = 'password'):
        MQTTClient.__init__(self, client_id=client_name, server=server,
                            port=port, user=user, password=password)
        self._name = client_name
        self.start()

    def start(self):
        self.connect()
        # TODO add topic "home/{name}"
        self.publish(topic="init_devices",
                     msg="Hello from esp: " + modules.helpers.get_ip())
