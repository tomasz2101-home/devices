import os
import json


class Secrets:
    def __init__(self, file_name: str):

        self._config_file_name = "configuration/" + file_name
        self.secrets = {}  # type: ignore
        self.get()

    def get(self):
        file = open(self._config_file_name, 'r')
        self.secrets = json.loads(file.read())
        file.close()


class Config:
    def __init__(self, file_name: str):

        self._config_file_name = "configuration/" + file_name

        self.config = {}  # type: ignore
        self.get()

    def get(self):
        file = open(self._config_file_name, 'r')
        self.config = json.loads(file.read())
        file.close()

    def sensors_list(self):
        available_sensors = []
        for item in self.config['sensors'].keys():
            try:
                if self.config['sensors'][item]['enable'] or self.config['sensors'][item]['fake']:
                    available_sensors.append(item)
            except KeyError:
                pass
        return available_sensors
