import time
import lib.logging as logging
import modules.dht
import modules.newmqtt
import modules.configuration
import modules.wifi
import modules.helpers
import modules.leds
import modules.soil
import ds18b20
from modules.hardware import GenericBoard

class Device:
    def __init__(self, log_level: str, config_file: str, secrets_file: str):

        self.log_level = log_level
        self.configuration = modules.configuration.Config(
            file_name=config_file)
        self.config = self.configuration.config
        self.secrets = modules.configuration.Secrets(
            file_name=secrets_file).secrets

        self.board = GenericBoard(config=self.config)

        self._wifi = modules.wifi.WIFI(
            enabled=self.config["wifi"]["enable"], ssid=self.secrets["wifi"]["name"], password=self.secrets["wifi"]["password"])
        self._name = self.config['name']

        self._previous_time = time.time()
        self._interval = 1 * 60
        # self._interval = 5
        self._sensors = {}  # type: ignore
        self._sensors['list'] = self.configuration.sensors_list()

    def sensors_config(self):
        try:
            self._sensors['dht'] = modules.dht.FakeDHT()
            if self.config['sensors']['dht']['enable']:
                self._sensors['dht'] = modules.dht.DHT(
                    pin=self.board.get_pin(self.config['sensors']['dht']['pin']))
        except KeyError as e:
            logging.warning("No DHT configuration: %s", e)
        try:
            self._sensors['ds18b20'] = ds18b20.FakeDS18X20()
            if self.config['sensors']['ds18b20']['enable']:
                self._sensors['ds18b20'] = ds18b20.DS18X20(
                    pin=self.config['sensors']['ds18b20']['pin'])
        except KeyError as e:
            logging.warning("No ds18b20 configuration: %s", e)

        try:
            self._sensors['leds'] = modules.leds.FakeLeds()
            if self.config['sensors']['leds']['enable']:
                self._sensors['leds'] = modules.leds.Leds(
                    pin=self.config['sensors']['leds']['pin'],
                    leds_number=self.config['sensors']['leds']['number'])
        except KeyError as e:
            logging.warning("No Leds configuration: %s", e)

        try:
            self._sensors['soil'] = modules.soil.FakeMoisture()
            if self.config['sensors']['soil']['enable']:
                self._sensors['soil'] = modules.soil.Moisture(
                    pin=self.board.get_pin(self.config['sensors']['soil']['pin']),
                    moisture_range=self.config['sensors']['soil']['range'])
        except KeyError as e:
            logging.warning("No Soil configuration: %s", e)

    def setup(self):
        modules.helpers.start_time = time.time()
        logging.basicConfig(level=self.log_level)

        print("")
        logging.info("Setting up device ...")

        self._wifi.connect()
        modules.helpers.no_debug()
        # modules.helpers.set_time()
        self.sensors_config()
        try:
            self.mqtt = modules.newmqtt.FakeMyMQTT()
            if self.config['mqtt']['enable']:
                self.mqtt = modules.newmqtt.MyMQTT(
                    client_name=self.config['name'],
                    server=self.config['mqtt']["server"],
                    port=self.config['mqtt']["port"],
                    user=self.secrets['mqtt']["user"],
                    password=self.secrets['mqtt']["password"])
        except KeyError as e:
            logging.error("No Mqtt configuration: %s", e)

        logging.info("--- %.2f seconds ---",
                     modules.helpers.since_start_seconds())

    def run(self):
        logging.info("Starting...")
        # self._mqtt.check_connection()
        logging.info('Sensors list: {}'.format(self._sensors['list']))
        if 'leds' in self._sensors['list']:
            # self._mqtt.message_callback_add('home/{name}/led_01/set'.format(name=self._name),
            #                                 self.run_leds_mode)
            self.mqtt.set_callback(self.run_leds_mode)
            self.mqtt.subscribe(
                'home/{name}/led_01/set'.format(name=self._name))

        sensors = dict()
        while True:
            self._wifi.connect()
            if (time.time() - self._previous_time) >= self._interval:
                self._previous_time = self._previous_time + self._interval
                if 'dht' in self._sensors['list']:
                    sensors['dht'] = self._sensors['dht'].read_x_times(1, 1)
                if 'ds18b20' in self._sensors['list']:
                    sensors['ds18b20'] = self._sensors['ds18b20'].get_temperature()
                if 'soil' in self._sensors['list']:
                    sensors['soil'] = self._sensors['soil'].read_x_times(repeats=2, delay = 5)
                # if 'pms' in self._sensors_list:
                #     sensors['pms'] = self._air_quality_sensor.get_pm_ug_per_m3()
                if sensors:
                    self.mqtt.send_message(
                        topic='home/{name}/sensors'.format(name=self._name),
                        message='Sensors: {}'.format(
                            '/'.join(list(sensors.keys()))),
                        data=sensors)
            self.mqtt.check_msg()

    def run_leds_mode(self, topic: bytes, msg: bytes):
        topic_str = str(topic, 'utf-8')
        if topic_str == "home/{name}/led_01/set".format(name=self._name):
            payload = modules.helpers.message_to_dict(message=msg)
            msg_str = str(msg, 'utf-8')
            self._sensors['leds'].setup(config=payload)
            return_topic = topic_str[:-len('/set')]
            if payload['state'] == 'ON':
                logging.debug('Leds turn on')
                self._sensors['leds'].turn_on()
                self.mqtt.publish(topic=return_topic,
                                  msg=msg_str)
            elif payload['state'] == 'OFF':
                logging.debug('Leds turn off')
                self._sensors['leds'].turn_off()
                self.mqtt.publish(topic=return_topic,
                                  msg=msg_str)
            else:
                logging.warning("topic: {topic} unkown msg: {payload}".format(topic=topic_str,
                                                                              payload=payload))
        else:
            print(topic)
