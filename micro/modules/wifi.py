import network
import time


class WIFI:
    def __init__(self, enabled: bool, ssid: str, password: str):
        self._enabled = enabled
        self._ssid = ssid
        self._password = password
        self._reconnect_info = True
        self._connected_info = True

    def connect(self):
        try:
            if self._enabled:
                sta_if = network.WLAN(network.STA_IF)
                if not sta_if.isconnected():
                    print('Connecting to {} ...'.format(self._ssid))
                    sta_if.active(True)
                    sta_if.connect(self._ssid,
                                   self._password)
                    while not sta_if.isconnected():
                        if self._reconnect_info:
                            print('Waiting for connection...')
                            self._reconnect_info = False
                        time.sleep(1)

                    self._reconnect_info = True
                    self._connected_info = True
                else:
                    if self._connected_info:
                        print('Connected')
                        self._connected_info = False
            else:
                ap_if = network.WLAN(network.AP_IF)
                ap_if.active(False)
        except KeyError:
            print("Invalid wifi config")
