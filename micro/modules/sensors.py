from machine import Pin
import time


class Leds:
    def turn_on(self):
        p0 = Pin(2, Pin.OUT)    # create output pin on GPIO0
        time.sleep(1)
        p0.on()                 # set pin to "on" (high) level
        time.sleep(1)
        p0.off()                # set pin to "off" (low) level
