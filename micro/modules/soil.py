import time
import logging
import modules.helpers

try:
    import machine  # type: ignore
except ImportError:
    logging.warning("Skipping importing some libararies")

class FakeMoisture:
    def read_x_times(self, repeats: int = 10, delay: int = 2) -> dict:
        moisture = 0
        for _ in range(0, repeats):
            moisture += 5
        return {'fake_moisture': moisture / repeats}


class Moisture:
    def __init__(self, pin, moisture_range):
        self._sensor = machine.ADC(pin)
        self._sensor.atten(machine.ADC.ATTN_11DB)
        self._moisture_range = moisture_range

    def read_x_times(self, repeats: int = 3, delay: int = 5) -> dict:
        try:
            moisture = 0
            for _ in range(0, repeats):
                test = self._sensor.read()
                moisture += test
                time.sleep(delay)
        except OSError as e:
            logging.error('Failed to read sensor: {msg}'.format(msg=e))
        moisture = modules.helpers.translate(value=(moisture / repeats), range_in=self._moisture_range, range_out=[0,100])
        return {'moisture': int(moisture)}