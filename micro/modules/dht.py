import time
import logging

try:
    import dht  # type: ignore
    # from machine import Pin  # type: ignore
except ImportError:
    logging.warning("Skipping importing some libararies")


class FakeDHT:
    def read_x_times(self, repeats: int = 10, delay: int = 2) -> dict:
        humidity = 0
        temperature = 0
        for _ in range(0, repeats):
            humidity += 5
            temperature += 55
        return {'fake_temperature': temperature / repeats, 'fake_humidity': humidity / repeats}


class DHT:
    def __init__(self, pin):
        self._sensor = dht.DHT11(pin=pin)

    def read_x_times(self, repeats: int = 10, delay: int = 5) -> dict:
        try:
            humidity = 0
            temperature = 0
            for _ in range(0, repeats):
                self._sensor.measure()
                humidity += self._sensor.humidity()
                temperature += self._sensor.temperature()
                # how fast can i measure temperature ? this can be a problem for mqtt callbacks
                if delay > 1:
                    time.sleep(delay)
        except OSError as e:
            logging.error('Failed to read sensor: {msg}'.format(msg=e))
        return {'temperature': temperature / repeats, 'humidity': humidity / repeats}
